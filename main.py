import pygame as pg
import numpy as np
from ball import *


def new_ball(dict, max_balls, posx, posy, vx, vy):
    for key in dict:
        ball = dict[key]
        if ball == None:
            new_ball = Ball(key, posx, posy, vx, vy)
            dict[key] = new_ball
            print(f"New ball created with id: {key}")
            break

def remove_ball(dict, key):
    dict[key] = None

def run_game(window):
    running  = True
    timer = pg.time.Clock()

    pg.font.init()
    font = pg.font.SysFont("Fire Sans Book", 60)

    win_init_width, win_init_height = window.get_size()

    fps = 120
    dt = 1 / fps

    max_balls = 1
    balls_dict = dict.fromkeys(range(1,max_balls+1))

    pad_height = win_init_height / 10
    pad_thickness = 20

    dy = pad_height / 2 * win_init_height

    left_points = 0
    right_points = 0

    left_pad_y_scale = 0.5
    right_pad_y_scale = 0.5

    ball_out = True

    while running:
        for event in pg.event.get():
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP and win_height * (right_pad_y_scale - dy) >= 0:
                    right_pad_y_scale -= dy
                elif event.key == pg.K_DOWN and win_height * (right_pad_y_scale + dy) <= win_height - pad_height / 2:
                    right_pad_y_scale += dy
                elif event.key == pg.K_w and win_height * (left_pad_y_scale - dy) >= 0:
                    left_pad_y_scale -= dy
                elif event.key == pg.K_s and win_height * (left_pad_y_scale + dy) <= win_height - pad_height / 2:
                    left_pad_y_scale += dy
            elif event.type == pg.QUIT:
                running = False

        win_width, win_height = window.get_size()

        window.fill((0, 0, 0))

        v0 = win_width / 10
        pad_height = win_height / 10
        dy = pad_height / win_height

        width_r = win_width / win_init_width
        height_r = win_height / win_init_height

        if width_r != 1:
            win_init_width = win_width
            for key in balls_dict:
                ball = balls_dict[key]
                if ball != None:
                    ball.vx *= width_r

        if height_r != 1:
            win_init_height = win_height
            for key in balls_dict:
                ball = balls_dict[key]
                if ball != None:
                    ball.vy *= width_r

        left_pad = pg.Surface((pad_thickness, pad_height))
        right_pad = pg.Surface((pad_thickness, pad_height))

        left_pad.fill((0, 255, 0))
        right_pad.fill((0, 255, 0))

        left_pad_rect = left_pad.get_rect(center=(pad_thickness / 2, win_height * left_pad_y_scale))
        right_pad_rect = right_pad.get_rect(center=(win_width - pad_thickness / 2, win_height * right_pad_y_scale))

        window.blit(left_pad, left_pad_rect)
        window.blit(right_pad, right_pad_rect)

        if ball_out == True:
            posx = win_width / 2
            posy = win_height / 2

            theta = -np.pi / 4 + np.random.random() * np.pi / 2

            vx = (-1) ** np.random.randint(2) * v0 * np.cos(theta)
            vy = v0 * np.sin(theta)

            new_ball(balls_dict, max_balls, posx, posy, vx, vy)
            ball_out = False

        for key in balls_dict:
            ball = balls_dict[key]
            if ball != None:
                ball.update(dt, 0, 0)
                ball_circle = pg.draw.circle(window, (0, 255, 0), (ball.posx, ball.posy), pad_height/4)
                if ball_circle.colliderect(left_pad_rect) or ball_circle.colliderect(right_pad_rect):
                    print(f"Ball {key} hit a pad")
                    ball.vx *= -1.5
                    ball.vy *= 1.5
                elif ball.posx <= 0:
                    right_points += 1
                    remove_ball(balls_dict, key)
                    ball_out = True
                elif ball.posx >= win_width:
                    left_points += 1
                    remove_ball(balls_dict, key)
                    ball_out = True
                elif ball.posy <= 0 or ball.posy >= win_height:
                    print(f"Ball {key} hit the edges.")
                    ball.vy *= -1


        textbox_1 = font.render(f"Points: {right_points}", True, (0, 255, 0))
        textbox_1_rect = textbox_1.get_rect(topright=(win_width - 5, 5))
        window.blit(textbox_1, textbox_1_rect)

        textbox_2 = font.render(f"Points: {left_points}", True, (0, 255, 0))
        textbox_2_rect = textbox_2.get_rect(topleft=(5, 5))
        window.blit(textbox_2, textbox_2_rect)
        # Update our window
        pg.display.flip()
        timer.tick(fps)




def main():
    pg.init()
    window = pg.display.set_mode((1000, 1000), pg.RESIZABLE)

    run_game(window)

if __name__ == "__main__":
    main()
