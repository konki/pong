class Ball:
    def __init__(self, id, posx, posy, vx, vy):
        self.id = id
        self.posx = posx
        self.posy = posy
        self.vx = vx
        self.vy = vy

    def update(self, dt, ax, ay):
        self.vx += ax*dt
        self.vy += ay*dt

        self.posx += self.vx*dt
        self.posy -= self.vy*dt

class Target:
    def __init__(self, id, posx, posy):
        self.id = id
        self.posx = posx
        self.posy = posy

    def update(self, dx, dy):
        self.posx += dx
        self.posy += dy
